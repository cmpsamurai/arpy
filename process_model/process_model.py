#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
# Author :Moustafa Mahmoud Fathy (CMPSamurai) moustafa@cmpsamurai.com
# Updated Tuesday Feb 21 2017 
#


import re

DEBUG=False

PROBLEM_CLASSES={}
PROBLEM_CLASSES_MAPPING={}
PROBLEM_CLASSES_MAPPING_KEYS = []

UNIQUE_CLASSES=[]

'''
Defines Replace Dictionary for non unique Type Groupings , this is the only part
that depends on manually feeded information.
'''
REPLACE_DICT={
    "ABSOLUTELY_SCHEDULED_TIMINGSType":"TTCAN_ABSOLUTELY_SCHEDULED_TIMINGS",
    "ABSOLUTELY_SCHEDULED_TIMINGSType2134":"FLEXRAY_ABSOLUTELY_SCHEDULED_TIMINGS",
    "ARGUMENTSType3985":"AUTOSAR_OPERATION_ARGUMENT_INSTANCES",
    "ARGUMENTSType3355":"RUNNABLE_ENTITY_ARGUMENTS",
    "ARGUMENTSType685":"ARRAY_VALUE_SPECIFICATIONS",
    "ARGUMENTSType598":"ARGUMENT_DATA_PROTOTYPES",
    "ARRAY_ELEMENT_MAPPINGSType3460":"SENDER_REC_ARRAY_ELEMENT_MAPPINGS",
    "ARRAY_ELEMENT_MAPPINGSType":"CLIENT_SERVER_ARRAY_ELEMENT_MAPPINGS",
    "COMPLEX_TYPE_MAPPINGType3456":"SENDER_REC_ARRAY_TYPE_MAPPINGS",
    "COMPLEX_TYPE_MAPPINGType":"CLIENT_SERVER_ARRAY_TYPE_MAPPING",
    "CONNECTORSType":"SW_CONNECTORS",
    "CONNECTORSType1643":"COMMUNICATION_CONNECTORS",
    "CONTAINERSType1794":"ECUC_CONTAINER_VALUES",
    "CONTAINERSType":"ECUC_CONTAINERS",
    "DATA_ELEMENTSType3482":"VARIABLE_DATA_PROTOTYPES",
    "DATA_ELEMENTSType1245":"DIAGNOSTIC_DATA_ELEMENTS",
    "DATA_ELEMENTSType":"DIAGNOSTIC_PARAMETERS",
    "DATA_PROTOTYPE_REFType3155":"DATA_PROTOTYPE_DESTS",
    "DATA_PROTOTYPE_REFType":"DATA_TYPE_IN_SYSTEM_REFS",
    "DATA_MAPPINGSType4237":"DATA_PROTOTYPE_MAPPINGS",
    "DATA_MAPPINGSType":"COMMUNICATIONS_DATA_MAPPINGS",
    "DATA_PROTOTYPE_REFType":"DATA_TYPE_IN_SYSTEM_REFS",
    "DATA_PROTOTYPE_REFType3155":"DATA_PROTOTYPE_REFS",
    "DATA_SEND_POINTSType":"BSW_CALLED_ENTITY_DATA_SEND_POINTS",
    "DATA_SEND_POINTSType3356":"VARIABLE_ACCESS_DATA_SEND_POINTS",
    "DATA_TRANSFORMATIONSType2359":"DATA_TRANSFORMATION_REF_CONDITIONAL",
    "DATA_TRANSFORMATIONSType":"DATA_TRANSFORMATIONS",
    "DNS_SERVER_ADDRESSESType":"IP4_DNS_SERVER_ADDRESSES",
    "DNS_SERVER_ADDRESSESType2505":"IP6_DNS_SERVER_ADDRESSES",
    "ELEMENTSType":"AR_PACKAGE_ELEMENTS",
    "ELEMENTSType101":"APPLICATION_RECORD_DATA_TYPE_ELEMENTS",
    "ELEMENTSType134":"ARRAY_VALUE_SPECIFICATION_ELEMENTS",
    "ELEMENTSType965":"DIAGNOSTIC_CONTRIBUTION_SET_ELEMENTS",
    "EVENTSType":"BSW_INTERNAL_BEHAVIOR_EVENTS",
    "EVENTSType1378":"DIAGNOSTIC_RESPONSE_ON_EVENTS",
    "EVENTSType3770":"SWC_INTERNAL_BEHAVIOR_EVENTS",
    "EXCLUSIVE_AREA_POLICYSType":"BSW_INTERNAL_BEHAVIOR_EXCLUSIVE_AREA_POLICYS",
    "EXCLUSIVE_AREA_POLICYSType3771":"SWC_INTERNAL_BEHAVIOR_EXCLUSIVE_AREA_POLICYS",
    "I_SIGNAL_TRIGGERINGSType":"I_SIGNAL_TRIGGERINGS",
    "I_SIGNAL_TRIGGERINGSType3067":"I_SIGNAL_TRIGGERING_REF_CONDITIONALS",
    "TP_CONNECTIONSType":"CAN_TP_CONNECTIONS",
    "TP_CONNECTIONSType1564":"DO_IP_TP_CONNECTIONS",
    "TP_CONNECTIONSType2083":"FLEXRAY_AR_TP_CONNECTIONS",
    "TP_CONNECTIONSType2166":"FLEXRAY_TP_CONNECTIONS",
    "TP_CONNECTIONSType2540":"J_1939_TP_CONNECTIONS",
    "TP_CONNECTIONSType2669":"LIN_TP_CONNECTIONS",
    "TP_CONNECTIONSType3598":"SOMEIP_TP_CONNECTIONS",
    "TP_NODESType":"CAN_TP_NODES",
    "TP_NODESType2091":"FLEXRAY_AR_TP_NODES",
    "TP_NODESType2168":"FLEXRAY_TP_NODES",
    "TP_NODESType2541":"J_1939_TP_NODES",
    "TP_NODESType2670":"LIN_TP_NODES",
    "TP_ADDRESSSType":"CAN_TP_ADDRESSES",
    "TP_ADDRESSSType2089":"TP_ADDRESSES",
    "TP_CHANNELSType":"CAN_TP_CHANNELS",
    "TP_CHANNELSType2090":"FLEXRAY_AR_TP_CHANNELS",
    "SW_AXIS_CONTSType":"RULE_BASED_AXIS_CONTS",
    "SW_AXIS_CONTSType113":"SW_AXIS_CONTS",
    "TP_ECUSType":"CAN_TP_ECUS",
    "TP_ECUSType2167":"FLEXRAY_TP_ECUS",
    "INTERNAL_BEHAVIORSType":"SWC_INTERNAL_BEHAVIORS",
    "INTERNAL_BEHAVIORSType356":"BSW_INTERNAL_BEHAVIORS",
    "INTERNAL_TRIGGERING_POINTSType":"BSW_INTERNAL_TRIGGERING_POINTS",
    "INTERNAL_TRIGGERING_POINTSType3357":"INTERNAL_TRIGGERING_POINTS",
    "MAPPINGSType":"CONSTANT_SPECIFICATION_MAPPINGS",
    "MAPPINGSType2028":"FM_FEATURE_MAP_ELEMENTS",
    "MAPPINGSType3886":"SYSTEM_MAPPINGS",
    "OPERATIONSType":"CLIENT_SERVER_INTERFACE_OPERATIONS",
    "OPERATIONSType651":"COMMON_SIGNAL_PATH_OPERATIONS",
    "PARAMETERSType":"ECUC_PARAMETERS",
    "PARAMETERSType3026":"PARAMETER_INTERFACE_PARAMETERS",
    "PDU_POOLSType":"N_PDU_POOLS",
    "PDU_POOLSType2164":"FLEXRAY_TP_PDU_POOLS",
    "PDU_TRIGGERINGSType":"CAN_FRAME_PDU_TRIGGERING",
    "PDU_TRIGGERINGSType527":"CAN_PHYSICAL_CHANNEL_PDU_TRIGGERINGS",
    "RECORD_ELEMENT_MAPPINGSType":"CLIENT_SERVER_RECORD_ELEMENT_MAPPINGS",
    "RECORD_ELEMENT_MAPPINGSType3470":"SENDER_REC_RECORD_ELEMENT_MAPPINGS",
    "SERVICE_DEPENDENCYSType":"BSW_SERVICE_DEPENDENCYS",
    "SERVICE_DEPENDENCYSType3775":"SWC_SERVICE_DEPENDENCYS",
    "MODE_IREFSType":"BSW_MODE_SWITCH_EVENT_IREFS",
    "MODE_IREFSType3807":"SWC_MODE_SWITCH_EVENT_IREFS",
    "OPERATION_MAPPINGSType":"CLIENT_SERVER_OPERATION_MAPPINGS",
    "OPERATION_MAPPINGSType591":"CLIENT_SERVER_OPERATION_BLUEPRINT_MAPPINGS",
    "SUB_CONTAINERSType":"ECUC_CONTAINER_VALUE_SUB_CONTAINERS",
    "SUB_CONTAINERSType1822":"ECUC_PARAM_CONF_CONTAINER_DEF_SUB_CONTAINERS",
    "SUB_ELEMENTSType":"IMPLEMENTATION_DATA_TYPE_SUB_ELEMENTS",
    "SUB_ELEMENTSType2707":"MC_DATA_INSTANCE_SUB_ELEMENTS"
    }


VALUE_CLASSES=[
    "BOOLEAN_VALUE_VARIATION_POINT",
    "NUMERICAL_VALUE_VARIATION_POINT",
    "POSITIVE_INTEGER_VALUE_VARIATION_POINT",
    "INTEGER_VALUE_VARIATION_POINT",
    "FLOAT_VALUE_VARIATION_POINT",
    "UNLIMITED_INTEGER_VALUE_VARIATION_POINT",
    "L_OVERVIEW_PARAGRAPH",
    "L_PARAGRAPH",
    "LIMIT",    
    ]

def get_class_name_from_def_line(line):
    return line.split(' ')[1].split('(')[0]

def get_correct_name_for_duplicate(old_name):
    if old_name.find('Type')==-1: return old_name
    return old_name[0:old_name.rfind("Type")]

def is_troubled_class_def(line):
    return line.startswith("class ") and line.find("Type")!=-1

def is_class_def(line):
    return line.startswith("class ") and line.find(":")!=-1

def is_class_end(line) :
    return line.startswith("# end class ")

def get_correct_name_for_troubled_class(name):
    #Class is Not A Problem    
    correct_name= get_correct_name_for_duplicate(name)        
    if not PROBLEM_CLASSES_MAPPING.has_key(name):
        return get_correct_name_for_duplicate(name)
    
    unique_class_name = PROBLEM_CLASSES_MAPPING[name]
    if REPLACE_DICT.has_key(unique_class_name):
        return REPLACE_DICT[unique_class_name]
    else:
        if len(PROBLEM_CLASSES[correct_name])==1:
            return correct_name
        return unique_class_name

def insert_if_unique(class_name,classBody):
    correct_class_name = get_correct_name_for_duplicate(class_name)
    # Havn't been seen before
    if not PROBLEM_CLASSES.has_key(correct_class_name) :
        PROBLEM_CLASSES[correct_class_name] = [classBody]
        if class_name.endswith("Type"):
            PROBLEM_CLASSES_MAPPING[class_name]=correct_class_name
        return True

    #iterate over all class variants , if different than all insert it
    new_class_lines = classBody.split('\n')
    NumberOfSimilar = 0
    #if class_name.find("ABSOLUTELY_SCHEDULED_TIMINGS")!=-1 : DEBUG=True
    for unique_copy in PROBLEM_CLASSES[correct_class_name]:
        copy_class_lines = unique_copy.split('\n')
        copy_class_name  = get_class_name_from_def_line(copy_class_lines[0])
        if len(new_class_lines) != len(copy_class_lines):
            #Different Class According to # of Lines
            if DEBUG : print "Different NUM OF LINES of class  %s (%d) : %s (%d) "%(correct_class_name,len(copy_class_lines) , class_name,len(new_class_lines))
            continue

        Different  =False
        for lineNum in range(len(copy_class_lines)):
            #Compare two classes line by line
            if re.sub("Type([\d]*)","",copy_class_lines[lineNum]) != re.sub("Type([\d]*)","",new_class_lines[lineNum]):
                #Different Class According To Line Content
                if DEBUG :
                    print "Different LINES CONTENT of class  %s (%d) : %s (%d) "%(correct_class_name,len(copy_class_lines) , class_name,len(new_class_lines))
                Different=True
                break
        if not Different :
            NumberOfSimilar+=1
            #TODO Group Identical Classes
            PROBLEM_CLASSES_MAPPING[class_name]=copy_class_name
        
    if NumberOfSimilar==0:
        PROBLEM_CLASSES[correct_class_name].append(classBody)
        if DEBUG:
            cfile=open("classes\\"+class_name+".py","w")
            cfile.write(classBody)
            cfile.close()
            cfile=open("classes\\"+copy_class_name+".py","w")
            cfile.write(unique_copy)
            cfile.close()            
        return True
    return False
            
            
    

def preprocess_lib_code(code_file_path):
    #Open Code File For reading
    with open(code_file_path,"r") as code_file:
        while True:
            line=code_file.readline()
            if not line : break
            # Process File Line by line
            if is_troubled_class_def(line):
                class_name=get_class_name_from_def_line(line)
                class_body=""
                #print class_name
                while not is_class_end(line):
                    class_body+=line
                    line=code_file.readline()
                    if not line : break
                #InserT class if it is different
                if insert_if_unique(class_name,class_body):
                    UNIQUE_CLASSES.append(class_name)
                class_body=None    

def process_lib_code(code_file_path,out_file_path):
    #Open Code File For reading
    output_file=open(out_file_path,"w")

    ALL_CLASSES_SECTION_REACHED = False
    ALL_CLASSES_SECTION_PROCESSED_ELEMENTS = []

    PROBLEM_CLASSES_MAPPING_KEYS = PROBLEM_CLASSES_MAPPING.keys()
    PROBLEM_CLASSES_MAPPING_KEYS.sort()
    PROBLEM_CLASSES_MAPPING_KEYS.reverse()

    PROCESSED_CLASSES=[]
    
    with open(code_file_path,"r") as code_file:
        while True:
            line=code_file.readline()
            if not line : break
            #Remove Prefix and namespace
            line=line.replace("namespace_='AR:'","namespace_=''")
            line=line.replace("namespacedef_='xmlns:AR=\"http://autosar.org/schema/r4.0\"'","namespacedef_=''")


            #Check for Duplicate Class And Skip Re Writing it :)
            if is_class_def(line):
                name = get_class_name_from_def_line(line)
                correct_name = get_correct_name_for_troubled_class(name)
                if correct_name in PROCESSED_CLASSES:
                    if DEBUG :
                        print "Skipping : ",name,correct_name
                    while not is_class_end(line):
                        line=code_file.readline()
                        if not line : break
                    line=code_file.readline()
                    continue
                else:
                    PROCESSED_CLASSES.append(correct_name)
                    

                
            if line.find("Type")!=-1:
                 for troubled_name in PROBLEM_CLASSES_MAPPING_KEYS:
                    #replace with correct name
                    line=line.replace(troubled_name,get_correct_name_for_troubled_class(troubled_name))
                    #To handle name_='SHORT-NAME-FRAGMENTSType512'
                    line=line.replace(troubled_name.replace('_','-'),get_correct_name_for_troubled_class(troubled_name).replace("_","-"))
                    #To handle strange sub name Tupe Case
                    line=line.replace(troubled_name.replace('Type',''),get_correct_name_for_duplicate(troubled_name))
                    line=line.replace(troubled_name.replace('_','-').replace('Type',''),get_correct_name_for_troubled_class(troubled_name).replace("_","-"))

            #Process __all__ elements section only adding Unique
            if line.startswith("__all__ = ["):
                ALL_CLASSES_SECTION_REACHED=True
                output_file.write(line)
                continue

            if ALL_CLASSES_SECTION_REACHED:
                #Check For The End Of File
                if line.startswith(']'):
                    print "Finished Processing File"
                    output_file.write(line)
                    break                    
                element = line.split('"')[1]
                if  element in ALL_CLASSES_SECTION_PROCESSED_ELEMENTS:
                    continue
                ALL_CLASSES_SECTION_PROCESSED_ELEMENTS.append(element)
            output_file.write(line)
    output_file.close()


        
def post_process_lib_code(input_file,output_file):
    #Open Code File For reading
    output_file=open(output_file,"w")
    found_utf8_line=False
    with open(input_file,"r") as code_file:
        while True:
            line=code_file.readline()
            if not line : break
            if not found_utf8_line:
                if line.find("outfile.write(self.value)")!=-1:
                    line=line.replace("self.value","self.value.encode(\"utf8\")")
                    found_utf8_line=True                    
            # Process File Line by line
            if is_class_def(line):
                class_name=get_class_name_from_def_line(line)
                #Check against Special Classes and Process it
                if class_name=="AUTOSAR":
                    print "Found Class AUTOSAR"
                    while line.find("showIndent(outfile, level, pretty_print)")==-1:
                        output_file.write(line)
                        line=code_file.readline()
                    output_file.write(line)
                    output_file.write("        xmlns=\"xmlns=\\\"http://autosar.org/schema/r4.0\\\"\"\n")
                    output_file.write("        xmlns_xsi =\"xmlns:xsi=\\\"http://www.w3.org/2001/XMLSchema-instance\\\"\"\n")
                    output_file.write("        xsi_schemaLocation=\"xsi:schemaLocation=\\\"http://autosar.org/schema/r4.0 AUTOSAR_4-3-0.xsd\\\"\"\n")
                    output_file.write("        outfile.write(\"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\"?>\\n\")\n")
                    #read next line and replace it
                    line=code_file.readline()
                    line=line.replace("'<%s%s%s' ","'<%s%s%s %s %s %s' ")
                    line=line.replace("(namespace_, name_, namespacedef_ and ' ' + namespacedef_ or '', )","(namespace_, name_, namespacedef_ and ' ' + namespacedef_ or '',xmlns,xmlns_xsi,xsi_schemaLocation )")
                    output_file.write(line)
                elif class_name in VALUE_CLASSES:
                    print "found class : ",class_name
                    while line.find("outfile.write('>%s' % (eol_, ))")==-1:
                        output_file.write(line)
                        line=code_file.readline()
                    line=line.replace("('>%s' % (eol_, ))","('>')")
                    output_file.write(line)
                    line=code_file.readline()
                    output_file.write(line)
                    # Skip Show Indent
                    line=code_file.readline()
                else:
                    output_file.write(line)
            else:
                output_file.write(line)
    output_file.close()
                

if __name__ == '__main__':
    #preprocess_lib_code("input\\autosar40.py")
    #process_lib_code("input\\autosar40.py","output\\processed_new_autosar40.py")
    post_process_lib_code("output\\processed_new_autosar40.py","output\\ready_autosar40.py")

